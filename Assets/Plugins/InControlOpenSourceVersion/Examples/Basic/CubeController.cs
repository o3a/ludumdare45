using System;
using System.Collections;
using UnityEngine;
using InControl;


namespace BasicExample
{
	public class CubeController : MonoBehaviour
	{
        //InputDevice inputDevice;

        //void Start ()
        //{
        //    inputDevice = InputManager.ActiveDevice;

        //    for (int i=0;  i < inputDevice.Controls.Length; i++)
        //    {
        //        Debug.Log("control = " + inputDevice.Controls[i].ToString());
        //    }
        //}

        public TextMesh textMesh;

		void Update()
		{
			// Use last device which provided input.
			var inputDevice = InputManager.ActiveDevice;

            // Rotate target object with left stick.
            transform.Rotate( Vector3.down,  500.0f * Time.deltaTime * inputDevice.Direction.X, Space.World );
			transform.Rotate( Vector3.right, 500.0f * Time.deltaTime * inputDevice.Direction.Y, Space.World );
            //transform.Rotate(Vector3.down, 500.0f * Time.deltaTime * inputDevice.LeftStickX, Space.World);
            //transform.Rotate(Vector3.right, 500.0f * Time.deltaTime * inputDevice.LeftStickY, Space.World);

            InputControl control1 = inputDevice.GetControl(InputControlType.Action1);
            InputControl control2 = inputDevice.GetControl(InputControlType.Action2);

            if (control1.WasPressed)
            {
                Debug.Log("Action 1 was pressed!");
                textMesh.text = "Action 1 pressed";
            }

            if (control1.WasReleased)
            {
                Debug.Log("Action 1 was released!");
                textMesh.text = "Action 1 released";
            }

            if (control2.WasPressed)
            {
                Debug.Log("Action 2 was pressed!");
                textMesh.text = "Action 2 pressed";
            }

            if (control2.WasReleased)
            {
                Debug.Log("Action 2 was released!");
                textMesh.text = "Action 2 released";
            }
        }
    }
}
