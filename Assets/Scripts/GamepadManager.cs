﻿using System;
using System.Collections;
using UnityEngine;
using InControl;

public enum DPAD { UP, DOWN, LEFT, RIGHT }

public class GamepadManager : MonoBehaviour
{
    public static GamepadManager Instance;

    public Action buttonStart;  // Start
    public Action button1;      // Action 1
    public Action button2;      // Action 2
    public Action<DPAD> dPad;

    private float previousX = 0f;
    private float previousY = 0f;

    private InputDevice inputDevice;
    private InputControl control1, control2, control3, control4, control5;

    void Awake ()
    {
        Instance = this;
    }

    void Update ()
    {
        inputDevice = InputManager.ActiveDevice;

        control1 = inputDevice.GetControl(InputControlType.Action1);
        control2 = inputDevice.GetControl(InputControlType.Action2);
        control3 = inputDevice.GetControl(InputControlType.Share);
        control4 = inputDevice.GetControl(InputControlType.Pause);
        control5 = inputDevice.GetControl(InputControlType.Select);

        if (control3.WasPressed || control4.WasPressed || control5.WasPressed)
        {
            buttonStart?.Invoke();
        }

        if (control1.WasPressed)
        {
            button1?.Invoke();
        }

        if (control2.WasPressed)
        {
            button2?.Invoke();
        }

        if (inputDevice.Direction.X < -0.5f && previousX > -0.5f)
        {
            dPad?.Invoke(DPAD.LEFT);
        }

        if (inputDevice.Direction.X > 0.5f && previousX < 0.5f)
        {
            dPad?.Invoke(DPAD.RIGHT);
        }

        if (inputDevice.Direction.Y > 0.5f && previousY < 0.5f)
        {
            dPad?.Invoke(DPAD.UP);
        }

        if (inputDevice.Direction.Y < -0.5f && previousY > -0.5f)
        {
            dPad?.Invoke(DPAD.DOWN);
        }

        previousX = inputDevice.Direction.X;
        previousY = inputDevice.Direction.Y;
    }
}
