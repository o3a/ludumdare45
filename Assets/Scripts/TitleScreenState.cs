﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreenState : MonoBehaviour
{
    public GameObject titleSprite;
    public GameObject startSprite;

    void OnEnable ()
    {
        GamepadManager.Instance.buttonStart += GameStates.Instance.StartGame;
    }

    void OnDisable ()
    {
        GamepadManager.Instance.buttonStart -= GameStates.Instance.StartGame;
    }

    void Update ()
    {
        float scale = 1f + (float)((Math.Floor(Time.realtimeSinceStartup * 2f) % 3) / 6f);
        titleSprite.transform.localScale = new Vector3(scale, scale, 1f);

        bool active = (Math.Floor(Time.realtimeSinceStartup * 4f) % 4 != 0);
        startSprite.gameObject.SetActive(active);
    }
}
