﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class GameStates : MonoBehaviour
{
    public static GameStates Instance;

    public TitleScreenState titleScreenState;
    public InGameState inGameState;
    public PausedState pausedState;

    private TitleScreenState titleScreenStateInstance;
    private InGameState inGameStateInstance;
    private PausedState pausedStateInstance;

    void Awake ()
    {
        Instance = this;
    }

    void Start ()
    {
        titleScreenStateInstance = Instantiate(titleScreenState);
        titleScreenStateInstance.transform.parent = transform;
    }

    public void StartGame ()
    {
        Destroy(titleScreenStateInstance.gameObject);
        inGameStateInstance = Instantiate(inGameState);
        inGameStateInstance.transform.parent = transform;
    }

    public void EndGame ()
    {
        Destroy(inGameStateInstance.gameObject);
        titleScreenStateInstance = Instantiate(titleScreenState);
        titleScreenStateInstance.transform.parent = transform;

        if (pausedStateInstance != null)
        {
            Destroy(pausedStateInstance.gameObject);
        }
    }

    public void PauseGame ()
    {
        inGameStateInstance.gameObject.SetActive(false);
        pausedStateInstance = Instantiate(pausedState);
        pausedStateInstance.transform.parent = transform;
    }

    public void ResumeGame ()
    {
        inGameStateInstance.gameObject.SetActive(true);
        Destroy(pausedStateInstance.gameObject);
    }
}
