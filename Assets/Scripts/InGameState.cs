﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameState : MonoBehaviour
{
    public GameEntityManager gameEntityManagerPrefab;
    private GameEntityManager gameEntityManagerInstance;

    public GridManager gridManagerPrefab;
    private GridManager gridManagerInstance;

    public Player playerPrefab;
    private Player playerInstance;

    void OnEnable ()
    {
        GamepadManager.Instance.dPad += DPadPressed;
        GamepadManager.Instance.buttonStart += GameStates.Instance.PauseGame;
        GamepadManager.Instance.button1 += ButtonPressedAction1;
        GamepadManager.Instance.button2 += ButtonPressedAction2;
    }

    void OnDisable ()
    {
        GamepadManager.Instance.dPad -= DPadPressed;
        GamepadManager.Instance.buttonStart -= GameStates.Instance.PauseGame;
        GamepadManager.Instance.button1 -= ButtonPressedAction1;
        GamepadManager.Instance.button2 -= ButtonPressedAction2;
    }

    void Start ()
    {
        gridManagerInstance = Instantiate(gridManagerPrefab);
        gridManagerInstance.transform.parent = transform;

        playerInstance = Instantiate(playerPrefab);
        playerInstance.transform.parent = transform;
    }

    void ButtonPressedAction1 ()
    {
        playerInstance.Action1();
    }

    void ButtonPressedAction2 ()
    {
        playerInstance.Action2();
    }

    void DPadPressed (DPAD direction)
    {
        playerInstance.MovePlayer(direction);
    }
}
