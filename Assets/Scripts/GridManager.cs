﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridManager : MonoBehaviour
{
    public static GridManager Instance;

    public GridLine lineHorizontalPrefab;
    public GridLine lineVerticalPrefab;

    public float gridSize = 0.5f;

    private List<GridLine> horizontalLines = new List<GridLine>();
    private List<GridLine> verticalLines = new List<GridLine>();

    void Awake ()
    {
        Instance = this;
    }

    void Start ()
    {
        MakeGrid();
    }

    void MakeGrid ()
    {
        for (float i = -4.75f; i <= 4.75f; i += gridSize)
        {
            GridLine newHorizontalLine = Instantiate(lineHorizontalPrefab);
            newHorizontalLine.transform.parent = transform;
            newHorizontalLine.transform.position = new Vector3(0f, i, 0f);
            horizontalLines.Add(newHorizontalLine);
        }

        for (float i = -4.75f; i <= 4.75f; i += gridSize)
        {
            GridLine newVerticalLine = Instantiate(lineVerticalPrefab);
            newVerticalLine.transform.parent = transform;
            newVerticalLine.transform.position = new Vector3(i, 0f, 0f);
            verticalLines.Add(newVerticalLine);
        }
    }
}
