﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public enum Direction { NONE, UP, DOWN, LEFT, RIGHT }
public enum MoveStyle { STRAIGHT, RANDOM }

public class Move : MonoBehaviour
{
    public Direction direction;
    public float initialMoveDelay = 0f;
    public float moveSpeed = 1f;

    private Vector3[] directions = new Vector3[] { Vector3.zero, new Vector3(0f, 1f, 0f), new Vector3(0f, -1f, 0f), new Vector3(-1f, 0f, 0f), new Vector3(1f, 0f, 0f) };

    // something with time or speed?

    async void Start ()
    {
        await Task.Delay(TimeSpan.FromSeconds(initialMoveDelay));
        MoveInDirection();
    }

    private void MoveInDirection ()
    {
        transform.localPosition += GridManager.Instance.gridSize * directions[(int)direction];
    }
}
